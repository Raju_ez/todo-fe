import {todoActions} from "../reducers/TodoReducer";

export const {
    changeInput,
    addTask,
    completeTask,
    getTasks,
    changeFilter,
    deleteCompletedTasks,
    deleteTask,
    updateTask,
    reOrderTasks,
} = todoActions;