import React from 'react';
import './App.css';
import Dashboard from "./components/Dashboard";

function App(): JSX.Element {

    return <Dashboard/>;
}

export default App;
