import React from "react";
import {H1} from "./Typography";
import Container from "@mui/material/Container";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import AddItem from "./AddItem";
import Filter from "./Filter";
import TaskList from "./TaskList";
import ClearTasks from "./ClearTasks";

const Dashboard = (): JSX.Element => {

    return (
        <Container maxWidth="sm" className="container">
            <H1>Todo</H1>
            <Card>
                <CardContent>
                    <List>
                        <ListItem><AddItem/></ListItem>
                        <ListItem><Filter/></ListItem>
                    </List>
                    <TaskList/>
                    <ClearTasks/>
                </CardContent>
            </Card>

        </Container>
    );
}

export default Dashboard;