import React, {useEffect, useState} from "react";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import ListItemIcon from "@mui/material/ListItemIcon";
import CheckOutlinedIcon from "@mui/icons-material/CheckOutlined";
import CircleOutlinedIcon from "@mui/icons-material/CircleOutlined";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import ListItemButton from "@mui/material/ListItemButton";
import ListItem from "@mui/material/ListItem";
import TextField from "@mui/material/TextField";
import {Todo} from "../reducers/TodoReducer";
import {completeTask, deleteTask, updateTask} from "../actions/TodoActions";
import {useAppDispatch} from "../libs/redux-hooks";

interface TaskProps {
    task: Todo,
    onDragOver(task: Todo): void,
    onDragEnd(task: Todo): void,
}

export default function Task(props: TaskProps): JSX.Element {

    const {task} = props;

    const dispatch = useAppDispatch();
    const [value, setValue] = useState("");
    const [editMode, setEditMode] = useState(false);

    useEffect(() => {
        setValue(task.value);
    }, [task.value]);

    const onCompleteClick = (task: Todo): void => {
        dispatch(completeTask(task));
    };

    const onDeleteTask = (task: Todo): void => {
        dispatch(deleteTask(task));
    };

    const onValueChane = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setValue(event.target.value);
    };

    const onDragOver = (): void => {
        props.onDragOver(task);
    };

    const onDragEnd = (): void => {
        props.onDragEnd(task);
    };

    const toggleEdit = (): void => {
        if (task.completed) return;

        setEditMode(!editMode);

        if (editMode) dispatch(updateTask({task, value}));
    };

    return (
        <ListItem
            secondaryAction={
                <IconButton
                    edge="end"
                    aria-label="delete"
                    title="Delete"
                    onClick={() => onDeleteTask(task)}
                    children={<DeleteIcon/>}
                />
            }
            onDragOver={onDragOver}
            onDragEnd={onDragEnd}
        >
            <ListItemIcon onClick={() => onCompleteClick(task)}>
                <IconButton disabled={task.completed}>
                    {task.completed ? <CheckOutlinedIcon/> : <CircleOutlinedIcon/>}
                </IconButton>
            </ListItemIcon>
            {editMode ?
                <ClickAwayListener onClickAway={toggleEdit}>
                    <TextField
                        variant="standard"
                        value={value}
                        onChange={onValueChane}
                        fullWidth
                        autoFocus
                    />
                </ClickAwayListener> :
                <ListItemButton
                    draggable={true}
                    onDoubleClick={toggleEdit}
                    children={task.value}
                    color={"secondary"}
                    disableGutters
                    selected={task.completed}
                />
            }
        </ListItem>
    );
}