import React from "react";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import {useAppSelector, useAppDispatch} from "../libs/redux-hooks";
import {Todo, Filter as FilterType} from "../reducers/TodoReducer";
import {changeFilter} from "../actions/TodoActions";

interface ChipProp {
    color: 'primary' | 'error' | 'success',
    label: string,
    filter: FilterType,
}

const Filter = (): JSX.Element => {
    const dispatch = useAppDispatch();

    const tasks: Array<Todo> = useAppSelector(state => state.todo.list);
    const filter = useAppSelector(state => state.todo.filter);

    const completed = tasks.filter(task => task.completed);
    const incompleteLength: number = tasks.length - completed.length;

    const chips: Array<ChipProp> = [
        {color: "primary", label: "All Tasks : " + tasks.length, filter: "all"},
        {color: "error", label: "Incomplete : " + incompleteLength, filter: "incomplete"},
        {color: "success", label: "Complete : " + completed.length, filter: "complete"},
    ];

    const onFilterChange = (chip: ChipProp): void => {
        dispatch(changeFilter(chip.filter));
    }

    return (
        <Stack direction="row" spacing={1}>
            {chips.map((chip: ChipProp) => {
                return (
                    <Chip
                        variant={chip.filter === filter ? "filled" : "outlined"}
                        key={chip.color}
                        size="small"
                        color={chip.color}
                        label={chip.label}
                        onClick={() => onFilterChange(chip)}
                    />
                );
            })}
        </Stack>
    );
};

export default Filter;