import React, {useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../libs/redux-hooks";
import Collapse from "@mui/material/Collapse";
import List from "@mui/material/List";
import {getTasks, reOrderTasks} from "../actions/TodoActions";
import {Todo} from "../reducers/TodoReducer";
import {TransitionGroup} from "react-transition-group";
import Task from "./Task";

export default function TaskList(): JSX.Element {

    const dispatch = useAppDispatch();
    const [draggedOn, setDraggedOn] = useState("");

    const list = useAppSelector(state => state.todo.list);
    const filter = useAppSelector(state => state.todo.filter);

    useEffect(() => {
        dispatch(getTasks());
    }, [dispatch]);

    const onDragOver = (task: Todo): void => {
        setDraggedOn(task.id);
    };

    const onDragEnd = (task: Todo): void => {
        dispatch(reOrderTasks({
            from: task.id,
            to: draggedOn,
        }));
    };

    const tasks = list.filter((task: Todo) => {
        return (
            filter === "all" ||
            (filter === "complete" && task.completed) ||
            (filter === "incomplete" && !task.completed)
        )
    });

    return (
        <List sx={{maxHeight: 400, overflow: 'auto'}}>
            <TransitionGroup>
                {tasks.map(task =>
                    <Collapse key={task.id}>
                        <Task
                            task={task}
                            onDragOver={onDragOver}
                            onDragEnd={onDragEnd}
                        />
                    </Collapse>
                )}
            </TransitionGroup>
        </List>
    );
}