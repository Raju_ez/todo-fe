import React from "react";
import styled from "@emotion/styled";

interface TypographyProp {
    children: any
}

const H1 = ({children}: TypographyProp): JSX.Element => {

    const Element = styled.h1`
        text-align: center;
        color: #ffffff
    `;

    return <Element>{children}</Element>;
}

export default H1;