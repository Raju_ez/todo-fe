import React from "react";
import TextField from "@mui/material/TextField";
import Fab from "@mui/material/Fab";
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import {useAppDispatch, useAppSelector} from "../libs/redux-hooks";
import {changeInput, addTask} from "../actions/TodoActions";

const AddItem = (): JSX.Element => {

    const input = useAppSelector(state => state.todo.input);
    const dispatch = useAppDispatch();

    const onInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        dispatch(changeInput(event.target.value));
    };

    const onSubmit = (): void => {
        dispatch(addTask());
    };

    return (
        <>
            <TextField
                label="Create Some Tasks ..."
                fullWidth
                sx={{marginRight: '2.5rem'}}
                onChange={onInputChange}
                value={input}
                onKeyPress={(event) => {
                    if (event.key === "Enter") onSubmit();
                }}
            />

            <Fab
                sx={{
                    position: 'absolute',
                    right: 0
                }}
                color="primary"
                aria-label="add"
                size="medium"
                children={<AddOutlinedIcon/>}
                onClick={onSubmit}
            />
        </>
    );
};

export default AddItem;