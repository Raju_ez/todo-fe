import React from "react";
import Button from "@mui/material/Button";
import {useAppDispatch} from "../libs/redux-hooks";
import {deleteCompletedTasks} from "../actions/TodoActions";

export default function ClearTasks(): JSX.Element {

    const dispatch = useAppDispatch();

    return (
        <Button
            variant="text"
            color="warning"
            fullWidth
            onClick={() => dispatch(deleteCompletedTasks())}
        >
            Clear Completed Tasks
        </Button>
    );
}