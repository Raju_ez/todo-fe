import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import {AppDispatch, RootState} from "../reducers/store";

// Replacement for useDispatch
export const useAppDispatch = () => useDispatch<AppDispatch>();
// Replacement for useSelector
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;