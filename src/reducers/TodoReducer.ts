import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {randId} from "../libs/helpers";

export interface Todo {
    id: string,
    value: string,
    completed: boolean,
}

export type Filter = "all" | "complete" | "incomplete";

interface TodoState {
    list: Array<Todo>,
    input: string,
    filter: Filter,
}

const initialState = {
    list: [],
    input: '',
    filter: "all"
} as TodoState;

// Just using localstorage as API
const persistTasks = (state: TodoState): void => {
    localStorage.setItem('todoList', JSON.stringify(state.list));
}

export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        getTasks(state) {
            state.list = JSON.parse(localStorage.getItem('todoList') || '[]');
        },
        changeInput(state, action: PayloadAction<string>) {
            state.input = action.payload;
        },
        addTask(state) {
            state.list.push({
                id: randId(),
                value: state.input,
                completed: false,
            });

            state.input = '';

            persistTasks(state);
        },
        completeTask(state, action: PayloadAction<Todo>) {
            state.list = state.list.map(task => {
                if (action.payload.id === task.id) task.completed = true;

                return task;
            });

            persistTasks(state);
        },
        changeFilter(state, action: PayloadAction<Filter>) {
            state.filter = action.payload;
        },
        updateTask(state, {payload}: PayloadAction<{task: Todo, value: string}>) {
            state.list = state.list.map(task => {
                if (task.id === payload.task.id) task.value = payload.value;
                return task;
            });

            persistTasks(state);
        },
        deleteTask(state, action: PayloadAction<Todo>) {
            state.list = state.list.filter(task => {
                return task.id !== action.payload.id
            });

            persistTasks(state);
        },
        deleteCompletedTasks(state) {
            state.list = state.list.filter(task => {
                return !task.completed;
            });

            persistTasks(state);
        },
        reOrderTasks(state, {payload}: PayloadAction<{from: string, to: string}>) {
            const ids: Array<string> = state.list.map(task => task.id);
            const from = ids.indexOf(payload.from);
            const to = ids.indexOf(payload.to);

            let tasks = [...state.list];
            const moving = {...tasks[from]};
            tasks.splice(from, 1);

            tasks.splice(to, 0, moving);

            state.list = tasks;
        }
    },
    extraReducers: {},
});

export const todoActions = todoSlice.actions;

export default todoSlice.reducer;